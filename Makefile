.PHONY: build test clean reconfigure

index.js: source/HelloWorld.js
	@ docker run --rm \
	-v `pwd`:/app \
	-w="/app" \
	node:12.6.0-alpine \
		npm install \
		&& npx babel source --out-dir ./

# test:
# 	@ docker run --rm \
# 	-v `pwd`:/app \
# 	-w="/app" \
# 	node:12.6.0-alpine \
# 		npm install \
# 		&& npx babel source test --out-dir ./ \
# 		&& node test.js

configure:
	docker run --rm \
	-v `pwd`:/app \
	-w="/app" \
	node:12.6.0-alpine \
		npm update

clean:
	@ docker run --rm \
	-v `pwd`:/app \
	-w="/app" \
	node:12.6.0-alpine \
		rm -rf node_modules *.js configure;

start-jenkins:
	docker run \
		--rm \
		-u root \
		-p 8888:8080 \
		-v jenkins-data:/var/jenkins_home \
		-v /var/run/docker.sock:/var/run/docker.sock \
		-v "$HOME":/home \
		jenkinsci/blueocean